## Welcome to mhoelzerKenzie.github.io 

You can use the [editor on GitHub](https://github.com/mhoelzer/mhoelzerKenzie.github.io/edit/master/index.md) to maintain and preview the content for your website in Markdown files.

Whenever you commit to this repository, GitHub Pages will run [Jekyll](https://jekyllrb.com/) to rebuild the pages in your site, from the content in your Markdown files.

<!DOCTYPE html>
<html>
	<head>
		<title>Images</title>
		<style type="text/css">
			.circle{
				border-radius: 50%;
			}
		</style>
	</head>
	<body>
		<h1>Images</h1>
		<h3>Img from internet source</h3>
		<img src="https://s-media-cache-ak0.pinimg.com/originals/6c/2e/1e/6c2e1e933aefec8a94bac9f0c2c2b264.jpg">
		<br>
		<h3>Linking to another page through an image</h3>
		<a href="https://en.wikipedia.org/wiki/Saxophone" target="_blank">
			<img src="https://s-media-cache-ak0.pinimg.com/originals/6c/2e/1e/6c2e1e933aefec8a94bac9f0c2c2b264.jpg">
		</a>
		<h3>Img in a circle</h3>
		<img src="https://i.imgur.com/NaejWqG.jpg" class="circle">
	</body>
</html>

### Markdown

Markdown is a lightweight and easy-to-use syntax for styling your writing. It includes conventions for

```markdown
Syntax highlighted code block

# Header 1
## Header 2
### Header 3

- Bulleted
- List

1. Numbered
2. List

**Bold** and _Italic_ and `Code` text

[Link](url) and ![Image](src)
```
